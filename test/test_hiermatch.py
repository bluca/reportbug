import unittest

from reportbug import hiermatch, exceptions

test_strings_list = ['Beautiful is better than ugly.',
                     'Explicit is better than implicit.',
                     'Simple is better than complex.',
                     'Complex is better than complicated.',
                     'Flat is better than nested.',
                     'Sparse is better than dense.']

test_hierarchy = [
    ('normal', ['had better work', 'makes funny noise']),
    ('serious', ['better listen to your children', 'please behave']),
    ('minor', ['oh, sorry', 'you can do better!']),
    ('wishlist', ['looks better than I thought', 'better is good']),
    ('critical', ['hehe', 'hoho']),
]


class TestHiermatch(unittest.TestCase):

    def test_egrep_list(self):
        res = hiermatch.egrep_list(None, '')
        self.assertIsNone(res)

        with self.assertRaises(exceptions.InvalidRegex):
            matches = hiermatch.egrep_list(test_strings_list, 42)

        matches = hiermatch.egrep_list(test_strings_list, 'better')
        self.assertEqual(len(matches), 6)
        self.assertEqual(matches, [0, 1, 2, 3, 4, 5])

    def test_egrep_hierarchy(self):
        res = hiermatch.egrep_hierarchy(test_hierarchy, 'better')
        self.assertEqual(res, [[0], [0], [1], [0, 1], []])

    def test_matched_hierarchy(self):
        res = hiermatch.matched_hierarchy(test_hierarchy, 'better')
        self.assertEqual(len(res), 4)
        self.assertEqual(res, [
            ('normal', ['had better work']),
            ('serious', ['better listen to your children']),
            ('minor', ['you can do better!']),
            ('wishlist', ['looks better than I thought', 'better is good']),
        ])
